import React from "react";
import SubTitleNote from "./SubTitleNote";

export default class SubTitle extends React.Component {
  render() {
    return (
      <h2>
        {this.props.text}
        <div>
          <SubTitleNote
            text="Manda mais repetição!"
            feeling={this.props.feeling}
          />
        </div>
      </h2>
    );
  }
}
