import React from "react";

export default class TitleNote extends React.Component {
  render() {
    return (
      <div>
        {this.props.text} {this.props.feeling}
      </div>
    );
  }
}
